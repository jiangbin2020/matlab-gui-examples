classdef HourPin < Pin
        
    methods
        function obj = HourPin(haxes, num,  center, radius, linewidth)
           % center = [0, 0];
           % radius = 0.6;
            limit = [0, 12];
           % linewidth = 3;
             obj = obj@Pin(haxes, num,  limit, center, radius, linewidth); 
        end
        
    end
    
end