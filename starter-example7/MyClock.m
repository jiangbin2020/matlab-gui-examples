classdef MyClock < handle
   properties
       timer
       hpin
       mpin
       spin
   end
   
   methods
       function obj = MyClock(ax, options)
           if nargin==1
              options = struct('lims', [-2 2 -2 2],...
                                'center',[ 0 0],...
                                'background', 'on');
           end
           
            axis(ax, 'off');
            axis(ax, 'equal');
            hold(ax, 'on');
            %% 绘制轮廓
            if strcmp(options.background, 'off') == 1
                T = 0:.01:2*pi;
                plot(1.5*sin(T), 1.5*cos(T), 'k', 'linewidth', 2);

                %% 绘制小时刻度
                for i  = [12 1: 11]
                    t =- i/12*2*pi +0.5*pi;
                    x = 1.35*cos(t);
                    y = 1.35*sin(t);
                    text(x,y, num2str(i)); 
                    x1 = 1.4*cos(t);
                    y1 = 1.4*sin(t);
                    x2 = 1.5*cos(t);
                    y2=1.5*sin(t);
                    plot([x1, x2], [y1, y2],  'k');
                end
                %% 绘制分钟刻度
                for i  = [60 1: 59]
                   t =- i/60*2*pi +0.5*pi;  
                    x1 = 1.45*cos(t);
                    y1 = 1.45*sin(t);
                    x2 = 1.5*cos(t);
                    y2=1.5*sin(t);
                    plot([x1, x2], [y1, y2],  'k');
                end
                axis(ax, [-2 2 -2 2]);
                w = 4;
                center = [0 0];
            else
                    im = imread('clock1.jpg');
                    im = flipud(im);
                    imshow(im, 'Parent', ax);
                    hold(ax, 'on');
                    [w, h, d] = size(im);
                    ax.YDir = 'normal';    
                    center = [w/2 w/2];
            end
            %% 绘制时分秒针 
            label = text(-0.65, -0.4, '2019-09-20 12:00:00');
            spin = SecondPin(ax, 0, center, 0.7*w/2, 3);
            mpin = MinutePin(ax, 0, center, 0.5*w/2, 6);
            hpin = HourPin(ax, 0, center, 0.3*w/2, 8);
            userdata = struct('hpin', hpin, 'mpin', mpin, 'spin', spin, 'label', label);
           
            obj.spin = spin;
            obj.mpin = mpin;
            obj.hpin = hpin;
           %% 初始化定时器
            obj.timer = timer(); 
            obj.timer.TimerFcn = {@obj.timer_callback, userdata};   % 刷新时钟
            obj.timer.ExecutionMode = 'fixedRate';  
       end
       
       function timer_callback(obj, s, event, userdata) 
                if ~isvalid(userdata.label) 
                   stop(s);
                   delete(s);
                   return
                end
                label = userdata.label;
                time = event.Data.time; 
                set(label, 'string', sprintf('%04d-%02d-%02d %02d:%02d:%02d', time(1), time(2), time(3), time(4), time(5),floor( time(6)) ));
                obj.hpin.update( floor(time(4)) +floor(time(5))/60 );
                obj.mpin.update( floor(time(5)) +floor(time(6))/60 );
                obj.spin.update( floor(time(6)) ) ; 
       end
       
       function start(obj)
            start(obj.timer);
       end  
   end
end