classdef Pin  < handle
    
    properties 
        limit;  % [0 60]
        h;  % 
        num;
        haxes;
        center;
        radius;
        angle;
        linewidth;
    end
    
    methods
        function obj = Pin(haxes, num, limit, center, radius, linewidth)
            obj.limit = limit;
            obj.haxes = haxes;
            obj.num = num; 
            obj.center = center;
            obj.radius = radius; 
            obj.linewidth= linewidth;
            obj.plot();
        end
        
        function  plot(obj)
            obj.angle = (-obj.num/obj.limit(2) +1/4 ) *2*pi;
            t = obj.angle;
            X = obj.center(1) + obj.radius * cos(t) * [-0.1,  1];
            Y = obj.center(2) + obj.radius * sin(t) * [ -0.1, 1];
             if(ishandle(obj.h)) 
                 set(obj.h, 'XData', X, 'YData', Y, 'LineWidth', obj.linewidth); 
             else 
                 axes(obj.haxes);
                 obj.h =  plot(X, Y, 'r-', 'linewidth', obj.linewidth);
             end 
        end
        
        function update(obj, num)
            obj.num = num;
            obj.plot(); 
        end
        
    end
    
end