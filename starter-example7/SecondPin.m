classdef SecondPin < Pin
        
    methods
        function obj = SecondPin(haxes, num, center, radius, linewidth)
                % center = [0, 0];
                % radius = 1.4;
                limit = [0, 60];
                % linewidth = 1;
                obj = obj@Pin(haxes, num,  limit, center, radius, linewidth); 
        end
        
    end
    
end