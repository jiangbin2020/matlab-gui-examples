classdef MinutePin < Pin
        
    
    methods
        function obj = MinutePin(haxes, num, center, radius, linewidth)
                    % center = [0, 0];
                    % radius = 1;
                    limit = [0, 60];
                    % linewidth = 2;
                    obj = obj@Pin(haxes, num,  limit, center, radius, linewidth); 
        end
        
    end
    
end