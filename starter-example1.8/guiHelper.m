function [hfig, handles] = guiHelper()
    hfig = figure('Position', [600 100 510 800],...
    'Name', 'GUI属性查看助手', 'NumberTitle', 'off', 'ToolBar', 'none', 'MenuBar', 'none');

    text = uicontrol(hfig, 'Style', 'text', 'Position', [10 740 100 50],...
                    'String', '控件名称');
    popup = uicontrol(hfig, 'Style', 'popupmenu', 'Position', [110 740 100 50]);
    data = load('doc.mat');
    keys = fieldnames(data.doc);
    key = keys{1};
    value = data.doc.(key).tableData;
    table = uitable(hfig, 'Data', value, 'Position', [10, 10, 500, 740]);
    table.ColumnName = {'属性', '属性值', '可选值'};
    table.ColumnWidth = {100 100 200};
    handles.figure = hfig;
    handles.table = table;
    handles.popup = popup;
    handles.text = text;
    handles.doc = data.doc;
    
    % popup callback
    popup.String = keys;
    popup.Callback = @(o, e)popup_callback(o,e, guidata(o));
    guidata(hfig, handles);
end


function popup_callback(hObject, eventdata, handles)
    k = hObject.String{hObject.Value};
    handles.table.Data = handles.doc.(k).tableData;
    
end