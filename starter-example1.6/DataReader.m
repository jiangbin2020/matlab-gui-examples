function varargout = DataReader(varargin)
% DATAREADER MATLAB code for DataReader.fig
%      DATAREADER, by itself, creates a new DATAREADER or raises the existing
%      singleton*.
%
%      H = DATAREADER returns the handle to a new DATAREADER or the handle to
%      the existing singleton*.
%
%      DATAREADER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DATAREADER.M with the given input arguments.
%
%      DATAREADER('Property','Value',...) creates a new DATAREADER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DataReader_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DataReader_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DataReader

% Last Modified by GUIDE v2.5 29-Apr-2020 20:16:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DataReader_OpeningFcn, ...
                   'gui_OutputFcn',  @DataReader_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DataReader is made visible.
function DataReader_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DataReader (see VARARGIN)

% Choose default command line output for DataReader
handles.output = hObject;
handles.uitable1.ColumnName = {'x坐标','y坐标', '置信度', '可见性'};
handles.line_color = 'b';
hold(handles.axes1, 'on');
grid(handles.axes1, 'on');
box(handles.axes1, 'on');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DataReader wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DataReader_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.data = importdata('data.txt'); % 从文件读取数据
handles.uitable1.Data = handles.data;

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%  绘图按钮
data = handles.uitable1.Data;
handles.line = plot(data(:,1), data(:,2), 'b-o');
handles.line.Color = handles.line_color;
handles.axes1.YDir = 'reverse';
guidata(hObject, handles);

% --- Executes when selected cell(s) is changed in uitable1.
function uitable1_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)
r = eventdata.Indices(1);
x = hObject.Data(r, 1);
y = hObject.Data(r, 2);
if isfield(handles, 'current_point')
    handles.current_point.XData = x;
    handles.current_point.YData = y;
else
    handles.current_point = plot(x, y, 'ro', 'MarkerSize', 15);
end
guidata(hObject, handles);


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles) 
    color = choosedialog;
    s = struct('Red', 'r', 'Blue', 'b', 'Green', 'g');
    handles.line_color = s.(color);
    if isfield(handles, 'line')
       handles.line.Color = handles.line_color; 
    end
    guidata(hObject, handles);

