function [f, handles] =  contextmenu_demo() 
    f = figure('WindowStyle','normal');
    ax = axes;
    x = 0:100;
    y = x.^2;
    plotline = plot(x,y);
    c = uicontextmenu; 
    plotline.UIContextMenu = c; % Assign the uicontextmenu to the plot line 
    % Create child menu items for the uicontextmenu
    m1 = uimenu(c,'Label','dashed','Callback',@(o,e)setlinestyle(o,e, guidata(o)));
    m2 = uimenu(c,'Label','dotted','Callback',@(o,e)setlinestyle(o,e, guidata(o)));
    m3 = uimenu(c,'Label','solid','Callback',@(o,e)setlinestyle(o,e, guidata(o)));
    handles.ax = ax;
    handles.plotline = plotline;
    handles.m1 = m1;
    handles.m2 = m2;
    handles.m3 = m3;
    guidata(f, handles);
end

function setlinestyle(source,callbackdata, handles)
    plotline = handles.plotline;
    switch source.Label
        case 'dashed'
            plotline.LineStyle = '--';
        case 'dotted'
            plotline.LineStyle = ':';
        case 'solid'
            plotline.LineStyle = '-';
    end
end 