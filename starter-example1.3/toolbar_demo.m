function [f, handles] = toolbar_demo()
    f  = figure('ToolBar','none');
    t = uitoolbar(f);

    % Read an image
    [img,map] = imread(fullfile(matlabroot,...
                'toolbox','matlab','icons','matlabicon.gif'));

    % Convert image from indexed to truecolor
    icon = ind2rgb(img,map);

    % Create a uipushtool in the toolbar
    p = uipushtool(t,'TooltipString','Toolbar push button',...
                     'ClickedCallback', @btn_callback);

    % Set the button icon
    p.CData = icon;
    
    handles.toolbar = t;
    handles.t_button1 = p;
    guidata(f, handles);
end

function btn_callback(o, e)
    msgbox('Clicked uipushtool.', '��ʾ');
end