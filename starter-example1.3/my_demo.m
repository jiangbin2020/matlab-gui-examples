function varargout = my_demo(varargin)
% MY_DEMO MATLAB code for my_demo.fig
%      MY_DEMO, by itself, creates a new MY_DEMO or raises the existing
%      singleton*.
%
%      H = MY_DEMO returns the handle to a new MY_DEMO or the handle to
%      the existing singleton*.
%
%      MY_DEMO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MY_DEMO.M with the given input arguments.
%
%      MY_DEMO('Property','Value',...) creates a new MY_DEMO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before my_demo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to my_demo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help my_demo

% Last Modified by GUIDE v2.5 20-Apr-2020 16:04:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @my_demo_OpeningFcn, ...
                   'gui_OutputFcn',  @my_demo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before my_demo is made visible.
function my_demo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to my_demo (see VARARGIN)

% Choose default command line output for my_demo
handles.output = hObject;
%%  变量定义
handles.im = [];   % 存放图片
%% 菜单栏
    handles.menu_file = uimenu(hObject, 'Text', '文件');
    handles.menu_help = uimenu(hObject, 'Text', '说明', 'Callback', 'msgbox("图片查看器v0.3.","说明");');
    handles.menu_file_open_root = uimenu(handles.menu_file, 'Text', '打开'); 
    handles.menu_file_open = uimenu(handles.menu_file_open_root, 'Text', '图片'); 
    handles.menu_file_open.MenuSelectedFcn = @(o, e)file_open_Fcn(o, e, guidata(o));
%% 工具栏
    handles.toolbar = uitoolbar(hObject); 
    [A,map,transparency] = imread(fullfile(matlabroot,'toolbox','matlab','icons','file_open.png'));  
    transparency = double(transparency);
    transparency(transparency>0) = 1;
    transparency(transparency==0) = nan;
    icon = im2double(A).*transparency;
    handles.toolbar_file_open = uipushtool(handles.toolbar,'TooltipString','打开图片'); 
    handles.toolbar_file_open.CData = icon;
    handles.toolbar_file_open.ClickedCallback = @(o,e) file_open_Fcn(o, e, guidata(o));
%% 右键工具栏
    c = uicontextmenu;  
    handles.figure1.UIContextMenu = c; 
    % Create child menu items for the uicontextmenu
    m1 = uimenu(c,'Label','灰度图(t>0.1)','Callback',@(o,e)setImStyle(o,e, guidata(o)));
    m2 = uimenu(c,'Label','灰度图(t>0.5)','Callback',@(o,e)setImStyle(o,e, guidata(o)));
    m3 = uimenu(c,'Label','灰度图(t>0.7)','Callback',@(o,e)setImStyle(o,e, guidata(o)));
         
guidata(hObject, handles);

function setImStyle(hObject, e, handles) 
% 右键选项选中回调函数
    if isempty(handles.im)
        msgbox("未打开图像。", "提示");
        return;
    end

    switch hObject.Label
        case '灰度图(t>0.1)'
            threshhold = 0.1; 
        case '灰度图(t>0.5)'
            threshhold = 0.5; 
        case '灰度图(t>0.7)' 
            threshhold = 0.7; 
    end 
    im = im2double(handles.im);
    r = im(:,:,1);
    im = r>threshhold;
    imshow(im, 'Parent', handles.axes1);


function file_open_Fcn(hObject, eventdata, handles)
    choose_file(handles);


% --- Outputs from this function are returned to the command line.
function varargout = my_demo_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_path = handles.edit1.String;
imshow(img_path, 'Parent', handles.axes1);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectFileBtn.
function selectFileBtn_Callback(hObject, eventdata, handles)
% hObject    handle to selectFileBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% 记忆功能： 记忆上一次选择的目录
choose_file(handles); 

function choose_file(handles)
    if isfield(handles, 'last_dir')
        last_dir = handles.last_dir;
    else
        last_dir = '';
    end

    filter = [last_dir, '*.*'];
    [f, p] = uigetfile(filter, '选择图片');             % 弹窗选择图片文件，返回值f是文件名，p是所在目录

    % 如果用选择了文件，返回文件名f；如果用户取消选择，f=0        
    if f                        
       fullfilename = [p f];                            % 拼接得到完整路径
       im = imread(fullfilename);
       handles.edit1.String = fullfilename;             % 将完整路径显示显示在文本框中。
       imshow(im, 'Parent', handles.axes1);   % 在handles.axes1中显示图片。
       handles.im = im;
    end
    handles.last_dir = p;                       % 记忆用户选择的目录。
    guidata(handles.figure1, handles);

