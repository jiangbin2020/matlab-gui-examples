function [hfig, handles] = control_demo()
    hfig = figure('MenuBar','none', 'ToolBar','none',...
                'Name','控件示例界面', 'Position',[500, 500, 800, 200]);
    %% 面板1 
    panel1 = uipanel('Parent',hfig, 'Title', '计算', 'Position', [0, 0, 0.2, 1]); 
    % 静态文本
    text1 = uicontrol('Parent', panel1, 'Style','text',...
                'String', 'sin(x)', 'Position', [0, 140, 60, 30]); 
    % 输入框
    edit1 = uicontrol('Parent', panel1, 'Style','edit',...
                'String', '10', 'Position', [50, 150, 60, 30]);
    % 坐标系
    axes1 = axes('Parent', panel1, 'Position', [0.1, 0.1, 0.8, 0.5]); 
    % 普通按钮
    button1 = uicontrol('Parent', panel1, 'Style','pushbutton',...
                'String', '计算', 'Position', [50, 110, 60, 30],...
                'Callback',@(hObject, events)button1_Callback(hObject, events, guidata(hObject))); 
    function button1_Callback(hObject, events, handles) 
        % 普通按钮回调函数
        X = 1:.01:10;
        Y = sin(X);
        plot(X, Y, 'Parent', handles.axes1);
    end
    %% togglebutton面板 
    panel2 = uipanel('Parent',hfig, 'Title', 'togglebutton', 'Position', [0.2, 0, 0.2, 1]);
    tbutton1 = uicontrol('Parent', panel2, 'Style','togglebutton',...
                'String', '0', 'Position', [50, 150, 60, 30],...
                'Callback', @tbutton_callback); 
     function tbutton_callback(hobject, events)
        hobject.String = num2str(hobject.Value);
     end  
    %% radiobutton按钮面板 
    % panel3 = uipanel('Parent',hfig, 'Title', '其他', 'Position', [0.6, 0, 0.3, 1]);
    panel3 = uibuttongroup(hfig, 'Title', 'radiobutton',...
                  'Position',[0.4 0 .2 1],...
                  'SelectionChangedFcn',@(h,e)msgbox(e.NewValue.String));
    rbutton1 = uicontrol('Parent', panel3, 'Style', 'radiobutton', ...
                    'Position', [0, 20, 60, 20], 'String', 'rbutton1', 'value', 0); 
    rbutton2 = uicontrol('Parent', panel3, 'Style', 'radiobutton',...
                    'Position', [0, 40, 60, 20], 'String', 'rbutton2', 'value', 1); 
    rbutton3 = uicontrol('Parent', panel3, 'Style', 'radiobutton',...
                    'Position', [0, 60, 60, 20], 'String', 'rbutton3', 'value', 0); 
    %% 面板4：普通面板
    panel4 = uipanel('Parent',hfig, 'Title', 'togglebutton',...
                    'Position', [0.6, 0, 0.35, 1]);
    % checkbox 
    check1 = uicontrol('Parent', panel4, 'Style', 'checkbox',...
                    'String', '限制', 'Position',[10, 30, 60, 20],...
                    'Callback', @(hObject, events)check1_Callback(hObject, events, guidata(hObject))); 
    function check1_Callback(hObject, events, handles)
       if hObject.Value 
           msgbox("启用限制", "提示");
       else
           msgbox("取消限制", "提示");
       end
    end
    % 滑动条
    slider1 = uicontrol('Parent', panel4, 'Style', 'slider', ...
                    'String', '限制','min',0, 'max',100,...
                    'Position',[10, 60, 120, 20]);  
    slider1.Callback = @(hObject, events)slider1_Callback(hObject,...
                                                events, guidata(hObject));
    slider1_label = uicontrol('Parent', panel4, 'Style', 'text', ... 
                    'String', '0', 'Position',[140, 60, 40, 20]);
    function slider1_Callback(hObject, events, handles)
        handles.slider1_label.String = num2str(hObject.Value);
    end

    % 下拉菜单 
    pop1 = uicontrol('Parent', panel4, 'Style', 'popupmenu',...
                    'String', {'sin(x)', 'cos(x)'}, 'Position',[10, 90, 120, 20]);
    pop1.Callback = @(hobject, events)msgbox(hobject.String{hobject.Value});
    
    % 列表框 
    list1 = uicontrol('Parent', panel4, 'Style', 'listbox',...
                    'String',  {'选项1', '选项2'}, 'Position',[10, 120, 120, 50]);
    list1.Callback = @(hobject, events)msgbox(hobject.String{hobject.Value});
    
    %% handles结构体数据
    handles.figure1 = hfig;
    handles.panel1 = panel1;
    handles.panel2 = panel2;
    handles.panel3 = panel3;
    handles.panel4 = panel4;
    handles.text1 = text1;
    handles.rbutton1 = rbutton1;
    handles.rbutton2 = rbutton2;
    handles.rbutton3 = rbutton3;
    handles.tbutton1 = tbutton1;
    handles.button1 = button1;
    handles.check1 = check1;
    handles.slider1 = slider1;
    handles.pop1 = pop1;
    handles.list1 = list1;
    handles.axes1 = axes1;
    handles.edit1 = edit1;
    handles.slider1_label = slider1_label;
    
    guidata(hfig, handles);
end